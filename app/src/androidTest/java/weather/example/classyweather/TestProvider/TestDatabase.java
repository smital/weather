package weather.example.classyweather.TestProvider;

import android.test.AndroidTestCase;

import weather.example.classyweather.provider.WeatherSQLiteOpenHelper;

/**
 * Created by smitald on 3/15/2016 for ClassyWeather.
 */
public class TestDatabase extends AndroidTestCase {

    public static final String LOG_TAG = TestDatabase.class.getSimpleName();

    // Since we want each test to start with a clean slate
    void deleteTheDatabase() {
        mContext.deleteDatabase(WeatherSQLiteOpenHelper.DATABASE_FILE_NAME);
    }

    /*
        This function gets called before each test is executed to delete the database.  This makes
        sure that we always have a clean test.
     */
    public void setUp() {
        deleteTheDatabase();
    }




}
