package weather.example.classyweather.Utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import timber.log.Timber;
import weather.example.classyweather.R;

/**
 * Created by smitald on 3/18/2016 for ClassyWeather.
 */
public class Utility {
    public static String formatDate(long dateInput) {
        String dateString = new SimpleDateFormat("EEEE").format(new Date(dateInput * 1000));
        Timber.d("Formatted Date Input " + dateInput + " Converted Date " + dateString);
        return dateString;
    }

    public static String formatTemperature(double temperature) {
        String suffix = "\u00B0";
        return String.format("%.1f%s", temperature, suffix);
    }

    public static int getIconResourceForWeatherCondition(int weatherId) {
        // Based on weather code data found at:
        // http://bugs.openweathermap.org/projects/api/wiki/Weather_Condition_Codes
        if (weatherId >= 200 && weatherId <= 232) {
            return R.drawable.ic_weather_windy_white_36dp;
        } else if (weatherId >= 300 && weatherId <= 321) {
            return R.drawable.ic_weather_rainy_white_36dp;
        } else if (weatherId >= 500 && weatherId <= 504) {
            return R.drawable.ic_weather_rainy_white_36dp;
        } else if (weatherId == 511) {
            return R.drawable.ic_weather_snowy_white_36dp;
        } else if (weatherId >= 520 && weatherId <= 531) {
            return R.drawable.ic_weather_pouring_white_36dp;
        } else if (weatherId >= 600 && weatherId <= 622) {
            return R.drawable.ic_weather_snowy_white_36dp;
        } else if (weatherId >= 701 && weatherId <= 761) {
            return R.drawable.ic_weather_fog_white_36dp;
        } else if (weatherId == 761 || weatherId == 781) {
            return R.drawable.ic_weather_windy_white_36dp;
        } else if (weatherId == 800) {
            return R.drawable.ic_weather_sunny_white_36dp;
        } else if (weatherId == 801) {
            return R.drawable.ic_weather_cloudy_white_36dp;
        } else if (weatherId >= 802 && weatherId <= 804) {
            return R.drawable.ic_weather_cloudy_white_36dp;
        }
        return -1;
    }

}
