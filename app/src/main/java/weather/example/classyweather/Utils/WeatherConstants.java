package weather.example.classyweather.Utils;

/**
 * Created by smitald on 3/23/2016 for ClassyWeather.
 */
public class WeatherConstants {
    public static final String DEFAULT_CITY = "London";
    public static final int LOADER_ID = 0;
    public static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    public static final int COUNT = 7;
    public static final String CITY_NAME = "City_Name";
    public static final String CITY_ID = "City_Id";
    public static final String CITY_URL = "City_url";
    public static final String STATUS_BAR_COLOR = "last_known_status_bar_color";
    public static final String ACTION_BAR_COLOR = "last_known_action_bar_color";
    public static final String PREFS_NAME = "city_pref";
    public static final String PREFS_KEY = "default_city";
}
