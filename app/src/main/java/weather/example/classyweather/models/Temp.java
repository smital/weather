
package weather.example.classyweather.models;

public class Temp {

    private float day;
    private float min;
    private float max;
    private float night;
    private float eve;
    private float morn;

    /**
     * 
     * @return
     *     The current_temp
     */
    public float getDay() {
        return day;
    }

    /**
     * 
     * @param day
     *     The current_temp
     */
    public void setDay(float day) {
        this.day = day;
    }

    /**
     * 
     * @return
     *     The min
     */
    public float getMin() {
        return min;
    }

    /**
     * 
     * @param min
     *     The min
     */
    public void setMin(float min) {
        this.min = min;
    }

    /**
     * 
     * @return
     *     The max
     */
    public float getMax() {
        return max;
    }

    /**
     * 
     * @param max
     *     The max
     */
    public void setMax(float max) {
        this.max = max;
    }

    /**
     * 
     * @return
     *     The night
     */
    public float getNight() {
        return night;
    }

    /**
     * 
     * @param night
     *     The night
     */
    public void setNight(float night) {
        this.night = night;
    }

    /**
     * 
     * @return
     *     The eve
     */
    public float getEve() {
        return eve;
    }

    /**
     * 
     * @param eve
     *     The eve
     */
    public void setEve(float eve) {
        this.eve = eve;
    }

    /**
     * 
     * @return
     *     The morn
     */
    public float getMorn() {
        return morn;
    }

    /**
     * 
     * @param morn
     *     The morn
     */
    public void setMorn(float morn) {
        this.morn = morn;
    }

}
