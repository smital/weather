package weather.example.classyweather;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import weather.example.classyweather.Utils.Utility;
import weather.example.classyweather.Utils.WeatherConstants;

/**
 * Created by smitald on 3/17/2016 for ClassyWeather.
 */
public class CityAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int ITEM_COUNT = 3;
    private static final int ITEM_HEADER = 0;
    private static final int ITEM_DETAIL = 1;
    private static final int ITEM_WIND_DETAIL = 2;

    private HeaderInfoViewHolder mHeaderInfo;
    private DetailInfoViewHolder mDetailInfo;
    private List<WeatherAdapterModel> mModelList = new ArrayList<>();
    private WindInfoViewHolder mWindInfo;
    public int lastPosition = -1;

    public CityAdapter() {
        setHasStableIds(true);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_HEADER:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_item, parent, false);
                mHeaderInfo = new HeaderInfoViewHolder(view);
                return mHeaderInfo;
            case ITEM_DETAIL:
                View viewDetail = LayoutInflater.from(parent.getContext()).inflate(R.layout.forecast_table, parent, false);
                mDetailInfo = new DetailInfoViewHolder(viewDetail);
                return mDetailInfo;
            case ITEM_WIND_DETAIL:
                View viewWind = LayoutInflater.from(parent.getContext()).inflate(R.layout.wind_pressure, parent, false);
                mWindInfo = new WindInfoViewHolder(viewWind);
                return mWindInfo;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case ITEM_HEADER:
                HeaderInfoViewHolder headerInfoViewHolder = (HeaderInfoViewHolder) holder;
                headerInfoViewHolder.bindHeaderInfo();
                lastPosition = position;
                break;
            case ITEM_DETAIL:
                DetailInfoViewHolder detailInfoViewHolder = (DetailInfoViewHolder) holder;
                detailInfoViewHolder.bindDetailInfo();
                lastPosition = position;
                break;
            case ITEM_WIND_DETAIL:
                WindInfoViewHolder windInfoViewHolder = (WindInfoViewHolder) holder;
                windInfoViewHolder.bindWindInfo(position);
        }
    }

    @Override
    public int getItemCount() {
        return ITEM_COUNT;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return ITEM_HEADER;
        } else if (position == 1){
            return ITEM_DETAIL;
        }
        else{
            return ITEM_WIND_DETAIL;
        }
    }

    public void updateDataSet(List<WeatherAdapterModel> weatherList) {
        mModelList = weatherList;
    }



    public void rotateAnimation(View view, int position) {
            Animation mRotateAnim = AnimationUtils.loadAnimation(view.getContext(), R.anim.rotate_anim);
            view.startAnimation(mRotateAnim);
            lastPosition = position;

    }


    public class WindInfoViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.wind_icon)
        ImageView windIcon;

        @Bind(R.id.wind_detail)
        TextView wind_detail;

        @Bind(R.id.pressure_detail)
        TextView pressure_detail;

        public WindInfoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindWindInfo(int position) {
            wind_detail.setText("Wind : " + mModelList.get(0).getSpeed());
            pressure_detail.setText("Pressure : " + mModelList.get(0).getPressure());
            rotateAnimation(windIcon, position);
        }

    }

    public class HeaderInfoViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.large)
        TextView large;
        @Bind(R.id.imageView)
        ImageView imageLeft;
        @Bind(R.id.max)
        TextView max;
        @Bind(R.id.imageView2)
        ImageView imageRight;
        @Bind(R.id.min)
        TextView min;
        @Bind(R.id.icon)
        ImageView icon;
        @Bind(R.id.short_desc)
        TextView short_desc;


        public HeaderInfoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        public void bindHeaderInfo() {
            if (mModelList.size() > 0) {
                large.setText(Utility.formatTemperature(mModelList.get(0).getCurrent_temp()));
                max.setText(Utility.formatTemperature(mModelList.get(0).getMin()));
                min.setText(Utility.formatTemperature(mModelList.get(0).getMax()));
                icon.setImageResource(Utility.getIconResourceForWeatherCondition(mModelList.get(0).getWeather_id()));
                short_desc.setText(mModelList.get(0).getShort_desc());
            }
        }
    }

    public class DetailInfoViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.r1_day)
        TextView r1Day;
        @Bind(R.id.r1_weather_icon)
        ImageView r1WeatherIcon;
        @Bind(R.id.r1_min_temp)
        TextView r1MinTemp;
        @Bind(R.id.r1_max_temp)
        TextView r1MaxTemp;
        @Bind(R.id.r2_day)
        TextView r2Day;
        @Bind(R.id.r2_weather_icon)
        ImageView r2WeatherIcon;
        @Bind(R.id.r2_min_temp)
        TextView r2MinTemp;
        @Bind(R.id.r2_max_temp)
        TextView r2MaxTemp;
        @Bind(R.id.r3_day)
        TextView r3Day;
        @Bind(R.id.r3_weather_icon)
        ImageView r3WeatherIcon;
        @Bind(R.id.r3_min_temp)
        TextView r3MinTemp;
        @Bind(R.id.r3_max_temp)
        TextView r3MaxTemp;
        @Bind(R.id.r4_day)
        TextView r4Day;
        @Bind(R.id.r4_weather_icon)
        ImageView r4WeatherIcon;
        @Bind(R.id.r4_min_temp)
        TextView r4MinTemp;
        @Bind(R.id.r4_max_temp)
        TextView r4MaxTemp;
        @Bind(R.id.r5_day)
        TextView r5Day;
        @Bind(R.id.r5_weather_icon)
        ImageView r5WeatherIcon;
        @Bind(R.id.r5_min_temp)
        TextView r5MinTemp;
        @Bind(R.id.r5_max_temp)
        TextView r5MaxTemp;


        public DetailInfoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindDetailInfo() {
            if (mModelList.size() == WeatherConstants.COUNT) {
                setDate();
                setIcon();
                setMax();
                setMin();
            }
        }

        private void setMin() {
            r1MaxTemp.setText(Utility.formatTemperature(mModelList.get(0).getMax()));
            r2MaxTemp.setText(Utility.formatTemperature(mModelList.get(1).getMax()));
            r3MaxTemp.setText(Utility.formatTemperature(mModelList.get(2).getMax()));
            r4MaxTemp.setText(Utility.formatTemperature(mModelList.get(3).getMax()));
            r5MaxTemp.setText(Utility.formatTemperature(mModelList.get(4).getMax()));
        }

        private void setMax() {
            r1MinTemp.setText(Utility.formatTemperature(mModelList.get(0).getMin()));
            r2MinTemp.setText(Utility.formatTemperature(mModelList.get(1).getMin()));
            r3MinTemp.setText(Utility.formatTemperature(mModelList.get(2).getMin()));
            r4MinTemp.setText(Utility.formatTemperature(mModelList.get(3).getMin()));
            r5MinTemp.setText(Utility.formatTemperature(mModelList.get(4).getMin()));
        }

        private void setIcon() {
            r1WeatherIcon.setImageResource(Utility.getIconResourceForWeatherCondition(mModelList.get(0).getWeather_id()));
            r2WeatherIcon.setImageResource(Utility.getIconResourceForWeatherCondition(mModelList.get(1).getWeather_id()));
            r3WeatherIcon.setImageResource(Utility.getIconResourceForWeatherCondition(mModelList.get(2).getWeather_id()));
            r4WeatherIcon.setImageResource(Utility.getIconResourceForWeatherCondition(mModelList.get(3).getWeather_id()));
            r5WeatherIcon.setImageResource(Utility.getIconResourceForWeatherCondition(mModelList.get(4).getWeather_id()));
        }

        private void setDate() {
            r1Day.setText(Utility.formatDate(mModelList.get(0).getDate()));
            r2Day.setText(Utility.formatDate(mModelList.get(1).getDate()));
            r3Day.setText(Utility.formatDate(mModelList.get(2).getDate()));
            r4Day.setText(Utility.formatDate(mModelList.get(3).getDate()));
            r5Day.setText(Utility.formatDate(mModelList.get(4).getDate()));
        }
    }

}
