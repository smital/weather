package weather.example.classyweather.background;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.util.Log;
import android.widget.Toast;

import de.greenrobot.event.EventBus;
import timber.log.Timber;
import weather.example.classyweather.Receiver.NetworkStateChanged;
import weather.example.classyweather.Utils.WeatherConstants;
import weather.example.classyweather.models.FlickrModel.FlickrData;
import weather.example.classyweather.models.FlickrModel.Photo;
import weather.example.classyweather.models.List;
import weather.example.classyweather.models.Weather;
import weather.example.classyweather.provider.location.LocationColumns;
import weather.example.classyweather.provider.location.LocationContentValues;
import weather.example.classyweather.provider.location.LocationCursor;
import weather.example.classyweather.provider.location.LocationSelection;
import weather.example.classyweather.provider.weather.WeatherColumns;
import weather.example.classyweather.provider.weather.WeatherContentValues;
import weather.example.classyweather.provider.weather.WeatherSelection;
import weather.example.classyweather.retrofit.FlickrService;
import weather.example.classyweather.retrofit.OpenWeatherMapService;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class WeatherUpdateService extends IntentService {
    private static final String CITY_NAME = "city_name";
    private static final String TAG = WeatherUpdateService.class.getSimpleName();
    private boolean mNetworkConnected = true;

    public WeatherUpdateService() {
        super("WeatherUpdateService");
    }

    public static void saveDefaultCity(Context context, String text) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(WeatherConstants.PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.putString(WeatherConstants.PREFS_KEY, text);
        editor.commit();
    }

    public static String getDefaultCity(Context context) {
        SharedPreferences settings;
        String text;
        settings = context.getSharedPreferences(WeatherConstants.PREFS_NAME, Context.MODE_PRIVATE);
        text = settings.getString(WeatherConstants.PREFS_KEY, null);
        return text;
    }


    /**
     * This method will be called when the network state changes through broadcast receiver
     *
     * @param event network event
     */
    public void onEvent(NetworkStateChanged event) {
        if (!event.isInternetConnected()) {
            Toast.makeText(this, "Network Detected !!", Toast.LENGTH_LONG).show();
            mNetworkConnected = false;
        } else {
            mNetworkConnected = true;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startService(Context context, String location) {
        Intent intent = new Intent(context, WeatherUpdateService.class);
        if (location != null) {
            intent.putExtra(CITY_NAME, location);
        }
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String city;
        FlickrData flickrData = null;
        Weather weatherData = null;

        /* Check if internet present */
        /* Check if there is data in "location" table */

        boolean var = isLocationTableEmpty();
        String text = getDefaultCity(this);

        city = intent.getStringExtra(CITY_NAME);

        if (mNetworkConnected && (city != null)) {
            addLocationEntry(city);
            loadOpenWeatherData(city);
        } else if (mNetworkConnected && isLocationTableEmpty() && (getDefaultCity(this) != null)) {
            city = getDefaultCity(this);
            addLocationEntry(city);
            loadOpenWeatherData(city);
        } else if (mNetworkConnected && (!isLocationTableEmpty())) {
            Log.d(TAG, "Fetching weather info");
            LocationSelection where = new LocationSelection();
            Cursor c = getContentResolver().query(LocationColumns.CONTENT_URI, LocationColumns.ALL_COLUMNS,
                    where.sel(), where.args(), null);

            LocationCursor location = new LocationCursor(c);
            while (c.moveToNext()) {
                city = location.getCityName();
                loadOpenWeatherData(city);
            }
        }

    }

    private void updateWeatherRecord(String city, Weather weather) {
        int location_id = getLocationId(city);
        java.util.List<List> weatherList = weather.getList();

        /* delete the rows in weather table */
        deleteRowsInWeatherTable(location_id);
        for (List item : weatherList) {
            fillWeatherTable(location_id, item);
        }
    }

    private void fillWeatherTable(int location_id, List item) {
        long date = item.getDt();
        String short_desc = item.getWeather().get(0).getDescription();
        int weather_id = item.getWeather().get(0).getId();
        float min = item.getTemp().getMin();
        float max = item.getTemp().getMax();
        float current = item.getTemp().getDay();
        float humidity = item.getHumidity();
        float pressure = item.getPressure();
        float wind = item.getSpeed();
        float degrees = item.getDeg();

        WeatherContentValues values = new WeatherContentValues();
        values.putLocationId(location_id)
                .putDate(date)
                .putShortDesc(short_desc)
                .putWeatherId(weather_id)
                .putMin(min)
                .putMax(max)
                .putHumidity(humidity)
                .putPressure(pressure)
                .putWind(wind)
                .putDegrees(degrees)
                .putCurrentTemp(current);

        getContentResolver().insert(values.uri(), values.values());
    }

    private void deleteRowsInWeatherTable(int location_id) {
        WeatherSelection where = new WeatherSelection();
        where.locationId(location_id);
        getContentResolver().delete(WeatherColumns.CONTENT_URI, where.sel(), where.args());
    }

    protected void loadOpenWeatherData(String city) {
        OpenWeatherMapService service = new OpenWeatherMapService();
        Weather weather = service.loadFlickrData(city);
        updateWeatherRecord(city, weather);
    }

    protected FlickrData addLocationEntry(String city) {
        FlickrData data;
        FlickrService service = new FlickrService();
        data = service.loadFlickrData(city);
        if (data.getPhotos().getPhoto().size() > 0)
            updateLocationTable(data, city);
        else {
            data = service.loadFlickrData(WeatherConstants.DEFAULT_CITY);
            updateLocationTable(data, city);
        }
        return data;
    }

    private boolean isLocationTableEmpty() {
        LocationSelection where = new LocationSelection();
        Cursor c = getContentResolver().query(LocationColumns.CONTENT_URI, LocationColumns.ALL_COLUMNS,
                where.sel(), where.args(), null);
        if (c == null) {
            c.close();
            return true;
        } else {
            if (c.getCount() == 0) {
                c.close();
                return true;
            }
            c.close();
            return false;
        }
    }

    private int getLocationId(String city) {
        LocationSelection where = new LocationSelection();
        where.cityName(city);
        int id = -1;
        Cursor c = getContentResolver().query(LocationColumns.CONTENT_URI, LocationColumns.ALL_COLUMNS,
                where.sel(), where.args(), null);

        LocationCursor location = new LocationCursor(c);
        if (c.moveToNext()) {
            id = (int) location.getId();
        }
        c.close();
        return id;
    }

    private String buildFlickrPicUrls(Photo photo) {
        String url = String.format("https://farm%d.staticflickr.com/%s/%s_%s_z.jpg",
                photo.getFarm(), photo.getServer(), photo.getId(), photo.getSecret());

        Timber.d(url);
        return url;
    }

    private void updateLocationTable(FlickrData flickrData, String city) {
        LocationSelection where = new LocationSelection();
        where.cityName(city);
        LocationContentValues values = new LocationContentValues();

        java.util.List<Photo> photo = flickrData.getPhotos().getPhoto();
        if (photo.size() > 0) {
            values.putUrl1(buildFlickrPicUrls(photo.get(0)))
                    .putUrl2(buildFlickrPicUrls(photo.get(1)))
                    .putUrl3(buildFlickrPicUrls(photo.get(2)))
                    .putCityName(city);
            getContentResolver().insert(LocationColumns.CONTENT_URI, values.values());
        } else {
            values.putCityName(city);
            getContentResolver().insert(LocationColumns.CONTENT_URI, values.values());
        }
    }
}
