package weather.example.classyweather;

import android.app.AlarmManager;
import android.app.LoaderManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.greenrobot.event.EventBus;
import timber.log.Timber;
import weather.example.classyweather.Receiver.NetworkStateChanged;
import weather.example.classyweather.Utils.WeatherConstants;
import weather.example.classyweather.Utils.NetWorkStatus;
import weather.example.classyweather.background.WeatherUpdateService;
import weather.example.classyweather.provider.PlaceInfoProvider;
import weather.example.classyweather.provider.location.LocationColumns;
import weather.example.classyweather.provider.location.LocationCursor;
import weather.example.classyweather.provider.location.LocationSelection;
import weather.example.classyweather.provider.weather.WeatherSelection;

import static android.provider.BaseColumns._ID;
import static weather.example.classyweather.provider.location.LocationColumns.CITY_NAME;

public class MainActivity extends AppCompatActivity implements PlaceInfoProvider.PlaceInfoCallback,
        LoaderManager.LoaderCallbacks<Cursor>, CityFragment.OnFragmentInteractionListener, ViewPager.OnPageChangeListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private PlaceInfoProvider mProvider;
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
    private List<String> mCityListLocationTable = new ArrayList<>();
    private static CoordinatorLayout coordinatorLayout;
    Map<String, String> mHmapUrl = new HashMap<>();
    Map<String, Integer> mHmapId = new HashMap<>();
    private TextView mNwError;
    private Menu mMenu;
    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;
    private Toolbar toolbar;

    /**
     * Id to identify a camera permission request.
     */
    private static final int REQUEST_FINE_LOCATION = 0;

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
                AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                        .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                        .build();

                Intent intent =
                        new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                                .setFilter(typeFilter)
                                .build(MainActivity.this);
                startActivityForResult(intent, WeatherConstants.PLACE_AUTOCOMPLETE_REQUEST_CODE);
            } catch (GooglePlayServicesRepairableException e) {
                // TODO: Handle the errorr
            } catch (GooglePlayServicesNotAvailableException e) {
                // TODO: Handle the error.
            }
        }
    };



    /**
     * This method will be called when the network state changes through broadcast receiver
     *
     * @param event network event
     */
    public void onEvent(NetworkStateChanged event) {
        if (!event.isInternetConnected()) {
            Toast.makeText(this, "No network Detected !!", Toast.LENGTH_LONG).show();
            mMenu.getItem(0).setVisible(true);
            mNwError.setVisibility(View.INVISIBLE);
        } else {
            if (viewPagerAdapter.getCount() == 0) {
                getCityInfo();
            }
            mNwError.setVisibility(View.INVISIBLE);
            mMenu.getItem(0).setVisible(false);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        mNwError = (TextView) findViewById(R.id.nwerror);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.laylout_label);
        setupViewPager();
        if (NetWorkStatus.getInstance(this).isOnline()) {
            getCityInfo();
        } else {
            mNwError.setVisibility(View.VISIBLE);
        }
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(mOnClickListener);
        getLoaderManager().initLoader(WeatherConstants.LOADER_ID, null, this);
        EventBus.getDefault().register(this);
        startAlarm();
    }

    public void startAlarm() {
        alarmMgr = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AlarmRepeatReceiver.class);
        alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0);

         /* Setting up inexact repeating alarm */
        alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                AlarmManager.INTERVAL_HALF_DAY,
                AlarmManager.INTERVAL_HALF_DAY, alarmIntent);

       /* int interval = 5000;
        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, alarmIntent);
        Toast.makeText(this, "Alarm Set", Toast.LENGTH_SHORT).show();*/

    }

    public void cancelAlarm() {
        // If the alarm has been set, cancel it.
        if (alarmMgr != null) {
            alarmMgr.cancel(alarmIntent);
        }
    }

    public void showerror() {
        mNwError.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        cancelAlarm();
    }

    private void getCityInfo() {
        connectToGooglePlayService();
        getDefaultCityAndStartService();
    }

    private void getDefaultCityAndStartService() {
        if (WeatherUpdateService.getDefaultCity(this) == null) {
            if (mProvider.getCurrentPlace() != null) {
                // Try to get the default city and store it in shared pref
                WeatherUpdateService.saveDefaultCity(this, mProvider.getCurrentPlace());
            } else {
                WeatherUpdateService.saveDefaultCity(this, WeatherConstants.DEFAULT_CITY);
            }
        } else {
            WeatherUpdateService.startService(this, null);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mProvider.disconnect();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        mMenu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String title;
        int id = item.getItemId();
        if (id == R.id.action_remove) {
            int ItemId = viewPager.getCurrentItem();
            if(ItemId >= 0) {
                title = viewPagerAdapter.getPageTitle(ItemId).toString();
                mHmapUrl.remove(title);
                mHmapId.remove(title);
                RemoveFragFromViewPager(ItemId);
                new RemoveLocationEntry().execute(title);
            }
            else{
                Toast.makeText(this, "Please add new city first",Toast.LENGTH_LONG).show();
            }
        }
       closeOptionsMenu();
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String city;
        if (requestCode == WeatherConstants.PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Timber.i("Place: " + place.getName());
                city = place.getName().toString();
                handleSelectedLocation(city);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Timber.i(status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == REQUEST_FINE_LOCATION) {
            Log.i(TAG, "Received response for location permission request.");

            // Check if the only required permission has been granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // location permission has been granted, preview can be displayed
                Log.i(TAG, "LOCATION permission has now been granted. Showing preview.");
                Snackbar.make(coordinatorLayout, R.string.permision_available_location,
                        Snackbar.LENGTH_SHORT).show();
                getDefaultCityAndStartService();
            } else {
                Log.i(TAG, "LOCATION permission was NOT granted.");
                Snackbar.make(coordinatorLayout, R.string.permissions_not_granted,
                        Snackbar.LENGTH_SHORT).show();
            }

        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this, LocationColumns.CONTENT_URI, LocationColumns.ALL_COLUMNS, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.d(TAG, "Receiving updates on the provider");
        if (!(data.getCount() > 0)) {
            return;
        }
        List<String> list = new ArrayList<>();
        String city;
        int id;
        mCityListLocationTable.clear();
        while (data.moveToNext()) {
            list.clear();
            city = data.getString(data.getColumnIndex(CITY_NAME));
            id = data.getInt(data.getColumnIndex(_ID));
            if (!mHmapId.containsKey(city)) {
                Timber.d(data.getString(data.getColumnIndex(CITY_NAME)));
                Timber.d(data.getString(data.getColumnIndex(LocationColumns.URL_1)));
                Timber.d(data.getString(data.getColumnIndex(LocationColumns.URL_2)));
                Timber.d(data.getString(data.getColumnIndex(LocationColumns.URL_3)));
                mCityListLocationTable.add(city);
                list.add(data.getString(data.getColumnIndex(LocationColumns.URL_1)));
                list.add(data.getString(data.getColumnIndex(LocationColumns.URL_2)));
                mHmapId.put(city, id);
                mHmapUrl.put(city, data.getString(data.getColumnIndex(LocationColumns.URL_1)));
                addFragToViewPager(city);
            } else {
                Timber.e("We alredy have " + city + " in the list");
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private void setupViewPager() {
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.addOnPageChangeListener(this);
        viewPager.setOffscreenPageLimit(5);
    }

    private void connectToGooglePlayService() {
        mProvider = new PlaceInfoProvider(this, this);
        mProvider.connect();
    }


    private void handleSelectedLocation(String name) {
        if(!mHmapId.containsKey(name)){
            WeatherUpdateService.startService(this, name);
        }
    }

    @Override
    public void handleDefaultPlace(Place place) {
        List<Address> addresses;
        Timber.d("Default Location " + place.getAddress());
        Geocoder gcd = new Geocoder(this, Locale.getDefault());
        try {
            addresses = gcd.getFromLocation(place.getLatLng().latitude, place.getLatLng().longitude, 1);
            if (addresses.size() > 0) {
                WeatherUpdateService.saveDefaultCity(this, addresses.get(0).getLocality());
                WeatherUpdateService.startService(this, null);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        CityFragment frag = ((CityFragment) viewPagerAdapter.getItem(position));
        frag.setStatusBarColor();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onFragmentInteraction(String city) {

    }

    private void addFragToViewPager(@NonNull String place) {
        if (!viewPagerAdapter.isFragmentPresent(place)) {
            Bundle args = new Bundle();
            CityFragment osFrag = new CityFragment();
            Timber.d(place + " : fragment added");
            populateArgs(place, args);
            osFrag.setArguments(args);
            viewPagerAdapter.addFragment(osFrag, place);
            viewPagerAdapter.notifyDataSetChanged();
            viewPager.setCurrentItem(viewPagerAdapter.getCount() - 1);
        } else {
            Timber.e("Fragment already present");
        }
    }

    private void populateArgs(@NonNull String place, Bundle args) {
        args.putString(WeatherConstants.CITY_NAME, place);
        args.putInt(WeatherConstants.CITY_ID, mHmapId.get(place));
        args.putString(WeatherConstants.CITY_URL, mHmapUrl.get(place));
    }

    private void RemoveFragFromViewPager(int index) {
            viewPagerAdapter.removeFrag(index);
            viewPagerAdapter.notifyDataSetChanged();
            viewPager.setCurrentItem(index - 1);
    }


    private class RemoveLocationEntry extends AsyncTask<String, Void, String> {

        private int getLocationId(String city){
            LocationSelection where = new LocationSelection();
            where.cityName(city);
            int id = -1;
            Cursor c = getContentResolver().query(LocationColumns.CONTENT_URI, LocationColumns.ALL_COLUMNS,
                    where.sel(), where.args(), null);

            LocationCursor location = new LocationCursor(c);
            if (c.moveToNext()){
                id = (int)location.getId();
            }
            c.close();
            return id;
        }

        private void deleteRowsInWeatherTable(int location_id) {
            WeatherSelection where = new WeatherSelection();
            where.locationId(location_id);
            where.delete(getContentResolver());
        }


        @Override
        protected String doInBackground(String... params) {
            int location_id = getLocationId(params[0]);
            deleteRowsInWeatherTable(location_id);
            LocationSelection select = new LocationSelection();
            select.cityName(params[0]);
            select.delete(getContentResolver());
            return params[0];
        }

        protected void onPostExecute(String city) {
            Toast.makeText(MainActivity.this, "Removed City : " + city, Toast.LENGTH_LONG).show();
        }

    }

    static class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();
        private FragmentManager mFragManager;

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
            mFragManager = fm;
        }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }

        /* Clear all the fragments */
        public void clearAll() {
            for (int i = 0; i < mFragments.size(); i++) {
                mFragManager.beginTransaction().remove(mFragments.get(i)).commit();
            }
            mFragments.clear();
        }

        public boolean isFragmentPresent(String title) {
            if (mFragmentTitles.contains(title)) {
                return true;
            } else {
                return false;
            }
        }

        public List<String> getFragTitles() {
            return mFragmentTitles;
        }

        public void removeFrag(int index) {
            mFragManager.beginTransaction().remove(mFragments.get(index)).commit();
            mFragments.remove(mFragments.get(index));
            mFragmentTitles.remove(index);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }


        @Override
        public int getItemPosition(Object object) {
            return FragmentStatePagerAdapter.POSITION_NONE;
        }
    }

}
