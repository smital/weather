package weather.example.classyweather.retrofit;

import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Query;
import weather.example.classyweather.models.FlickrModel.FlickrData;

/**
 * Created by smitald on 3/13/2016 for ClassyWeather.
 */
public class FlickrService {

     /*"https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20cricket.series.past&format=json&diagnostics=true&env=store%3A%2F%2F0TxIGQMQbObzvU4Apia0V0&callback="*/;

    /*https://api.flickr.com/services/rest?method=flickr.photos.search&license=4&tags=city&text=london&api_key=7740f5ced20d40bca9155c60ebfd8efd&format=json*/
    private static final String API_URL = "https://api.flickr.com";
    private static final String format = "json";
    private static final String method = "flickr.photos.search";
    private static final String license = "4";
    private static final String tags = "architecture";
    private static final String nojsoncallback = "1";
    private static final String api_key = "7740f5ced20d40bca9155c60ebfd8efd";
    private static final String outdoor = "0";

    /*
     * Define a service for getting forecast information using Retrofit by Square
     */
    public interface FlickrDataService {
        @GET("/services/rest")
        FlickrData GetFlickrData(
                @Query("method") String method,
                @Query("license") String license,
                @Query("tags") String tags,
                @Query("text") String text,
                @Query("api_key") String key,
                @Query("format") String format,
                @Query("nojsoncallback") String no_callback,
                @Query("geo_context") String outdoor
        );
    }

    /*
     * Create an async call to the forecast service
     */
    public FlickrData loadFlickrData(String text) {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(API_URL)
                .build();

        FlickrDataService service = restAdapter.create(FlickrDataService.class);
        return service.GetFlickrData(method, license, tags, text, api_key, format, nojsoncallback, outdoor);
    }

}
