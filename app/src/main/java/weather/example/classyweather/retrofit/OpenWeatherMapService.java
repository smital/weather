package weather.example.classyweather.retrofit;

import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Query;
import weather.example.classyweather.models.Weather;

/**
 * Created by smitald on 3/14/2016 for ClassyWeather.
 */
public class OpenWeatherMapService {
    /*"https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20cricket.series.past&format=json&diagnostics=true&env=store%3A%2F%2F0TxIGQMQbObzvU4Apia0V0&callback="*/;

    /*http://api.openweathermap.org/data/2.5/forecast/daily?q=94043,usa&mode=json&units=metric&cnt=7&APPID=4a0e7c90a22d219a09235eb58f404816*/

    private static final String API_URL = "http://api.openweathermap.org";
    private static final String mode = "json";
    private static final String units = "metric";
    private static final String cnt = "7";
    private static final String app_id = "4e4446319853d91ffd8d0bff3321bf71";


    /*
     * Define a service for getting forecast information using Retrofit by Square
     */
    public interface openWeatherMapDataService {
        @GET("/data/2.5/forecast/daily")
        Weather getOpenWeatherMapData(
                @Query("q") String city,
                @Query("mode") String mode,
                @Query("units") String units,
                @Query("cnt") String cnt,
                @Query("APPID") String app_id
        );
    }

    /*
     * Create an async call to the forecast service
     */
    public Weather loadFlickrData( String city) {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(API_URL)
                .build();

        openWeatherMapDataService service = restAdapter.create(openWeatherMapDataService.class);
        return service.getOpenWeatherMapData(city, mode, units, cnt, app_id);
    }
}
