package weather.example.classyweather;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import weather.example.classyweather.background.WeatherUpdateService;

public class AlarmRepeatReceiver extends BroadcastReceiver {
    private static final String TAG = AlarmRepeatReceiver.class.getSimpleName();

    public AlarmRepeatReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "We are scheduled", Toast.LENGTH_LONG).show();
        Log.d(TAG, "Scheduling weather update service" + System.currentTimeMillis());
        WeatherUpdateService.startService(context, null);
    }
}
