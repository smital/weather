package weather.example.classyweather.app;

import android.app.Application;
import android.os.StrictMode;

import timber.log.Timber;
import weather.example.classyweather.BuildConfig;

/**
 * Created by smitald on 3/15/2016 for ClassyWeather.
 */
public class WeatherApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .penaltyLog()
                    .penaltyDeath()
                    .penaltyLog()
                    .build());
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .penaltyDeathOnNetwork()
                    .build());

            Timber.plant(new Timber.DebugTree());
            Timber.d("Setting up Timber");
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

}
