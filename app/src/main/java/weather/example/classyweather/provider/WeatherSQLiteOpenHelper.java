package weather.example.classyweather.provider;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.DefaultDatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.util.Log;

import weather.example.classyweather.BuildConfig;
import weather.example.classyweather.provider.location.LocationColumns;
import weather.example.classyweather.provider.weather.WeatherColumns;

public class WeatherSQLiteOpenHelper extends SQLiteOpenHelper {
    private static final String TAG = WeatherSQLiteOpenHelper.class.getSimpleName();

    public static final String DATABASE_FILE_NAME = "classyweather.db";
    private static final int DATABASE_VERSION = 1;
    private static WeatherSQLiteOpenHelper sInstance;
    private final Context mContext;
    private final WeatherSQLiteOpenHelperCallbacks mOpenHelperCallbacks;

    // @formatter:off
    public static final String SQL_CREATE_TABLE_LOCATION = "CREATE TABLE IF NOT EXISTS "
            + LocationColumns.TABLE_NAME + " ( "
            + LocationColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + LocationColumns.CITY_NAME + " TEXT NOT NULL, "
            + LocationColumns.URL_1 + " TEXT, "
            + LocationColumns.URL_2 + " TEXT, "
            + LocationColumns.URL_3 + " TEXT "
            + " );";

    public static final String SQL_CREATE_TABLE_WEATHER = "CREATE TABLE IF NOT EXISTS "
            + WeatherColumns.TABLE_NAME + " ( "
            + WeatherColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + WeatherColumns.LOCATION_ID + " INTEGER NOT NULL, "
            + WeatherColumns.DATE + " INTEGER NOT NULL, "
            + WeatherColumns.SHORT_DESC + " TEXT NOT NULL, "
            + WeatherColumns.WEATHER_ID + " INTEGER NOT NULL, "
            + WeatherColumns.CURRENT_TEMP + " REAL NOT NULL, "
            + WeatherColumns.MIN + " REAL NOT NULL, "
            + WeatherColumns.MAX + " REAL NOT NULL, "
            + WeatherColumns.HUMIDITY + " REAL NOT NULL, "
            + WeatherColumns.PRESSURE + " REAL NOT NULL, "
            + WeatherColumns.WIND + " REAL NOT NULL, "
            + WeatherColumns.DEGREES + " REAL NOT NULL "
            + ", CONSTRAINT fk_location_id FOREIGN KEY (" + WeatherColumns.LOCATION_ID + ") REFERENCES location (_id) ON DELETE SET NULL"
            + " );";

    // @formatter:on

    public static WeatherSQLiteOpenHelper getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = newInstance(context.getApplicationContext());
        }
        return sInstance;
    }

    private static WeatherSQLiteOpenHelper newInstance(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            return newInstancePreHoneycomb(context);
        }
        return newInstancePostHoneycomb(context);
    }


    /*
     * Pre Honeycomb.
     */
    private static WeatherSQLiteOpenHelper newInstancePreHoneycomb(Context context) {
        return new WeatherSQLiteOpenHelper(context);
    }

    private WeatherSQLiteOpenHelper(Context context) {
        super(context, DATABASE_FILE_NAME, null, DATABASE_VERSION);
        mContext = context;
        mOpenHelperCallbacks = new WeatherSQLiteOpenHelperCallbacks();
    }


    /*
     * Post Honeycomb.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private static WeatherSQLiteOpenHelper newInstancePostHoneycomb(Context context) {
        return new WeatherSQLiteOpenHelper(context, new DefaultDatabaseErrorHandler());
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private WeatherSQLiteOpenHelper(Context context, DatabaseErrorHandler errorHandler) {
        super(context, DATABASE_FILE_NAME, null, DATABASE_VERSION, errorHandler);
        mContext = context;
        mOpenHelperCallbacks = new WeatherSQLiteOpenHelperCallbacks();
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        if (BuildConfig.DEBUG) Log.d(TAG, "onCreate");
        mOpenHelperCallbacks.onPreCreate(mContext, db);
        db.execSQL(SQL_CREATE_TABLE_LOCATION);
        db.execSQL(SQL_CREATE_TABLE_WEATHER);
        mOpenHelperCallbacks.onPostCreate(mContext, db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            setForeignKeyConstraintsEnabled(db);
        }
        mOpenHelperCallbacks.onOpen(mContext, db);
    }

    private void setForeignKeyConstraintsEnabled(SQLiteDatabase db) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            setForeignKeyConstraintsEnabledPreJellyBean(db);
        } else {
            setForeignKeyConstraintsEnabledPostJellyBean(db);
        }
    }

    private void setForeignKeyConstraintsEnabledPreJellyBean(SQLiteDatabase db) {
        db.execSQL("PRAGMA foreign_keys=ON;");
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void setForeignKeyConstraintsEnabledPostJellyBean(SQLiteDatabase db) {
        db.setForeignKeyConstraintsEnabled(true);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        mOpenHelperCallbacks.onUpgrade(mContext, db, oldVersion, newVersion);
    }
}
