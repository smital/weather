package weather.example.classyweather.provider.weather;

import java.util.Date;

import android.content.Context;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import weather.example.classyweather.provider.base.AbstractSelection;
import weather.example.classyweather.provider.location.*;

/**
 * Selection for the {@code weather} table.
 */
public class WeatherSelection extends AbstractSelection<WeatherSelection> {
    @Override
    protected Uri baseUri() {
        return WeatherColumns.CONTENT_URI;
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param contentResolver The content resolver to query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @return A {@code WeatherCursor} object, which is positioned before the first entry, or null.
     */
    public WeatherCursor query(ContentResolver contentResolver, String[] projection) {
        Cursor cursor = contentResolver.query(uri(), projection, sel(), args(), order());
        if (cursor == null) return null;
        return new WeatherCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, null)}.
     */
    public WeatherCursor query(ContentResolver contentResolver) {
        return query(contentResolver, null);
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param context The context to use for the query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @return A {@code WeatherCursor} object, which is positioned before the first entry, or null.
     */
    public WeatherCursor query(Context context, String[] projection) {
        Cursor cursor = context.getContentResolver().query(uri(), projection, sel(), args(), order());
        if (cursor == null) return null;
        return new WeatherCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(context, null)}.
     */
    public WeatherCursor query(Context context) {
        return query(context, null);
    }


    public WeatherSelection id(long... value) {
        addEquals("weather." + WeatherColumns._ID, toObjectArray(value));
        return this;
    }

    public WeatherSelection idNot(long... value) {
        addNotEquals("weather." + WeatherColumns._ID, toObjectArray(value));
        return this;
    }

    public WeatherSelection orderById(boolean desc) {
        orderBy("weather." + WeatherColumns._ID, desc);
        return this;
    }

    public WeatherSelection orderById() {
        return orderById(false);
    }

    public WeatherSelection locationId(int... value) {
        addEquals(WeatherColumns.LOCATION_ID, toObjectArray(value));
        return this;
    }

    public WeatherSelection locationIdNot(int... value) {
        addNotEquals(WeatherColumns.LOCATION_ID, toObjectArray(value));
        return this;
    }

    public WeatherSelection locationIdGt(int value) {
        addGreaterThan(WeatherColumns.LOCATION_ID, value);
        return this;
    }

    public WeatherSelection locationIdGtEq(int value) {
        addGreaterThanOrEquals(WeatherColumns.LOCATION_ID, value);
        return this;
    }

    public WeatherSelection locationIdLt(int value) {
        addLessThan(WeatherColumns.LOCATION_ID, value);
        return this;
    }

    public WeatherSelection locationIdLtEq(int value) {
        addLessThanOrEquals(WeatherColumns.LOCATION_ID, value);
        return this;
    }

    public WeatherSelection orderByLocationId(boolean desc) {
        orderBy(WeatherColumns.LOCATION_ID, desc);
        return this;
    }

    public WeatherSelection orderByLocationId() {
        orderBy(WeatherColumns.LOCATION_ID, false);
        return this;
    }

    public WeatherSelection locationCityName(String... value) {
        addEquals(LocationColumns.CITY_NAME, value);
        return this;
    }

    public WeatherSelection locationCityNameNot(String... value) {
        addNotEquals(LocationColumns.CITY_NAME, value);
        return this;
    }

    public WeatherSelection locationCityNameLike(String... value) {
        addLike(LocationColumns.CITY_NAME, value);
        return this;
    }

    public WeatherSelection locationCityNameContains(String... value) {
        addContains(LocationColumns.CITY_NAME, value);
        return this;
    }

    public WeatherSelection locationCityNameStartsWith(String... value) {
        addStartsWith(LocationColumns.CITY_NAME, value);
        return this;
    }

    public WeatherSelection locationCityNameEndsWith(String... value) {
        addEndsWith(LocationColumns.CITY_NAME, value);
        return this;
    }

    public WeatherSelection orderByLocationCityName(boolean desc) {
        orderBy(LocationColumns.CITY_NAME, desc);
        return this;
    }

    public WeatherSelection orderByLocationCityName() {
        orderBy(LocationColumns.CITY_NAME, false);
        return this;
    }

    public WeatherSelection locationUrl1(String... value) {
        addEquals(LocationColumns.URL_1, value);
        return this;
    }

    public WeatherSelection locationUrl1Not(String... value) {
        addNotEquals(LocationColumns.URL_1, value);
        return this;
    }

    public WeatherSelection locationUrl1Like(String... value) {
        addLike(LocationColumns.URL_1, value);
        return this;
    }

    public WeatherSelection locationUrl1Contains(String... value) {
        addContains(LocationColumns.URL_1, value);
        return this;
    }

    public WeatherSelection locationUrl1StartsWith(String... value) {
        addStartsWith(LocationColumns.URL_1, value);
        return this;
    }

    public WeatherSelection locationUrl1EndsWith(String... value) {
        addEndsWith(LocationColumns.URL_1, value);
        return this;
    }

    public WeatherSelection orderByLocationUrl1(boolean desc) {
        orderBy(LocationColumns.URL_1, desc);
        return this;
    }

    public WeatherSelection orderByLocationUrl1() {
        orderBy(LocationColumns.URL_1, false);
        return this;
    }

    public WeatherSelection locationUrl2(String... value) {
        addEquals(LocationColumns.URL_2, value);
        return this;
    }

    public WeatherSelection locationUrl2Not(String... value) {
        addNotEquals(LocationColumns.URL_2, value);
        return this;
    }

    public WeatherSelection locationUrl2Like(String... value) {
        addLike(LocationColumns.URL_2, value);
        return this;
    }

    public WeatherSelection locationUrl2Contains(String... value) {
        addContains(LocationColumns.URL_2, value);
        return this;
    }

    public WeatherSelection locationUrl2StartsWith(String... value) {
        addStartsWith(LocationColumns.URL_2, value);
        return this;
    }

    public WeatherSelection locationUrl2EndsWith(String... value) {
        addEndsWith(LocationColumns.URL_2, value);
        return this;
    }

    public WeatherSelection orderByLocationUrl2(boolean desc) {
        orderBy(LocationColumns.URL_2, desc);
        return this;
    }

    public WeatherSelection orderByLocationUrl2() {
        orderBy(LocationColumns.URL_2, false);
        return this;
    }

    public WeatherSelection locationUrl3(String... value) {
        addEquals(LocationColumns.URL_3, value);
        return this;
    }

    public WeatherSelection locationUrl3Not(String... value) {
        addNotEquals(LocationColumns.URL_3, value);
        return this;
    }

    public WeatherSelection locationUrl3Like(String... value) {
        addLike(LocationColumns.URL_3, value);
        return this;
    }

    public WeatherSelection locationUrl3Contains(String... value) {
        addContains(LocationColumns.URL_3, value);
        return this;
    }

    public WeatherSelection locationUrl3StartsWith(String... value) {
        addStartsWith(LocationColumns.URL_3, value);
        return this;
    }

    public WeatherSelection locationUrl3EndsWith(String... value) {
        addEndsWith(LocationColumns.URL_3, value);
        return this;
    }

    public WeatherSelection orderByLocationUrl3(boolean desc) {
        orderBy(LocationColumns.URL_3, desc);
        return this;
    }

    public WeatherSelection orderByLocationUrl3() {
        orderBy(LocationColumns.URL_3, false);
        return this;
    }

    public WeatherSelection date(int... value) {
        addEquals(WeatherColumns.DATE, toObjectArray(value));
        return this;
    }

    public WeatherSelection dateNot(int... value) {
        addNotEquals(WeatherColumns.DATE, toObjectArray(value));
        return this;
    }

    public WeatherSelection dateGt(int value) {
        addGreaterThan(WeatherColumns.DATE, value);
        return this;
    }

    public WeatherSelection dateGtEq(int value) {
        addGreaterThanOrEquals(WeatherColumns.DATE, value);
        return this;
    }

    public WeatherSelection dateLt(int value) {
        addLessThan(WeatherColumns.DATE, value);
        return this;
    }

    public WeatherSelection dateLtEq(int value) {
        addLessThanOrEquals(WeatherColumns.DATE, value);
        return this;
    }

    public WeatherSelection orderByDate(boolean desc) {
        orderBy(WeatherColumns.DATE, desc);
        return this;
    }

    public WeatherSelection orderByDate() {
        orderBy(WeatherColumns.DATE, false);
        return this;
    }

    public WeatherSelection shortDesc(String... value) {
        addEquals(WeatherColumns.SHORT_DESC, value);
        return this;
    }

    public WeatherSelection shortDescNot(String... value) {
        addNotEquals(WeatherColumns.SHORT_DESC, value);
        return this;
    }

    public WeatherSelection shortDescLike(String... value) {
        addLike(WeatherColumns.SHORT_DESC, value);
        return this;
    }

    public WeatherSelection shortDescContains(String... value) {
        addContains(WeatherColumns.SHORT_DESC, value);
        return this;
    }

    public WeatherSelection shortDescStartsWith(String... value) {
        addStartsWith(WeatherColumns.SHORT_DESC, value);
        return this;
    }

    public WeatherSelection shortDescEndsWith(String... value) {
        addEndsWith(WeatherColumns.SHORT_DESC, value);
        return this;
    }

    public WeatherSelection orderByShortDesc(boolean desc) {
        orderBy(WeatherColumns.SHORT_DESC, desc);
        return this;
    }

    public WeatherSelection orderByShortDesc() {
        orderBy(WeatherColumns.SHORT_DESC, false);
        return this;
    }

    public WeatherSelection weatherId(int... value) {
        addEquals(WeatherColumns.WEATHER_ID, toObjectArray(value));
        return this;
    }

    public WeatherSelection weatherIdNot(int... value) {
        addNotEquals(WeatherColumns.WEATHER_ID, toObjectArray(value));
        return this;
    }

    public WeatherSelection weatherIdGt(int value) {
        addGreaterThan(WeatherColumns.WEATHER_ID, value);
        return this;
    }

    public WeatherSelection weatherIdGtEq(int value) {
        addGreaterThanOrEquals(WeatherColumns.WEATHER_ID, value);
        return this;
    }

    public WeatherSelection weatherIdLt(int value) {
        addLessThan(WeatherColumns.WEATHER_ID, value);
        return this;
    }

    public WeatherSelection weatherIdLtEq(int value) {
        addLessThanOrEquals(WeatherColumns.WEATHER_ID, value);
        return this;
    }

    public WeatherSelection orderByWeatherId(boolean desc) {
        orderBy(WeatherColumns.WEATHER_ID, desc);
        return this;
    }

    public WeatherSelection orderByWeatherId() {
        orderBy(WeatherColumns.WEATHER_ID, false);
        return this;
    }

    public WeatherSelection currentTemp(float... value) {
        addEquals(WeatherColumns.CURRENT_TEMP, toObjectArray(value));
        return this;
    }

    public WeatherSelection currentTempNot(float... value) {
        addNotEquals(WeatherColumns.CURRENT_TEMP, toObjectArray(value));
        return this;
    }

    public WeatherSelection currentTempGt(float value) {
        addGreaterThan(WeatherColumns.CURRENT_TEMP, value);
        return this;
    }

    public WeatherSelection currentTempGtEq(float value) {
        addGreaterThanOrEquals(WeatherColumns.CURRENT_TEMP, value);
        return this;
    }

    public WeatherSelection currentTempLt(float value) {
        addLessThan(WeatherColumns.CURRENT_TEMP, value);
        return this;
    }

    public WeatherSelection currentTempLtEq(float value) {
        addLessThanOrEquals(WeatherColumns.CURRENT_TEMP, value);
        return this;
    }

    public WeatherSelection orderByCurrentTemp(boolean desc) {
        orderBy(WeatherColumns.CURRENT_TEMP, desc);
        return this;
    }

    public WeatherSelection orderByCurrentTemp() {
        orderBy(WeatherColumns.CURRENT_TEMP, false);
        return this;
    }

    public WeatherSelection min(float... value) {
        addEquals(WeatherColumns.MIN, toObjectArray(value));
        return this;
    }

    public WeatherSelection minNot(float... value) {
        addNotEquals(WeatherColumns.MIN, toObjectArray(value));
        return this;
    }

    public WeatherSelection minGt(float value) {
        addGreaterThan(WeatherColumns.MIN, value);
        return this;
    }

    public WeatherSelection minGtEq(float value) {
        addGreaterThanOrEquals(WeatherColumns.MIN, value);
        return this;
    }

    public WeatherSelection minLt(float value) {
        addLessThan(WeatherColumns.MIN, value);
        return this;
    }

    public WeatherSelection minLtEq(float value) {
        addLessThanOrEquals(WeatherColumns.MIN, value);
        return this;
    }

    public WeatherSelection orderByMin(boolean desc) {
        orderBy(WeatherColumns.MIN, desc);
        return this;
    }

    public WeatherSelection orderByMin() {
        orderBy(WeatherColumns.MIN, false);
        return this;
    }

    public WeatherSelection max(float... value) {
        addEquals(WeatherColumns.MAX, toObjectArray(value));
        return this;
    }

    public WeatherSelection maxNot(float... value) {
        addNotEquals(WeatherColumns.MAX, toObjectArray(value));
        return this;
    }

    public WeatherSelection maxGt(float value) {
        addGreaterThan(WeatherColumns.MAX, value);
        return this;
    }

    public WeatherSelection maxGtEq(float value) {
        addGreaterThanOrEquals(WeatherColumns.MAX, value);
        return this;
    }

    public WeatherSelection maxLt(float value) {
        addLessThan(WeatherColumns.MAX, value);
        return this;
    }

    public WeatherSelection maxLtEq(float value) {
        addLessThanOrEquals(WeatherColumns.MAX, value);
        return this;
    }

    public WeatherSelection orderByMax(boolean desc) {
        orderBy(WeatherColumns.MAX, desc);
        return this;
    }

    public WeatherSelection orderByMax() {
        orderBy(WeatherColumns.MAX, false);
        return this;
    }

    public WeatherSelection humidity(float... value) {
        addEquals(WeatherColumns.HUMIDITY, toObjectArray(value));
        return this;
    }

    public WeatherSelection humidityNot(float... value) {
        addNotEquals(WeatherColumns.HUMIDITY, toObjectArray(value));
        return this;
    }

    public WeatherSelection humidityGt(float value) {
        addGreaterThan(WeatherColumns.HUMIDITY, value);
        return this;
    }

    public WeatherSelection humidityGtEq(float value) {
        addGreaterThanOrEquals(WeatherColumns.HUMIDITY, value);
        return this;
    }

    public WeatherSelection humidityLt(float value) {
        addLessThan(WeatherColumns.HUMIDITY, value);
        return this;
    }

    public WeatherSelection humidityLtEq(float value) {
        addLessThanOrEquals(WeatherColumns.HUMIDITY, value);
        return this;
    }

    public WeatherSelection orderByHumidity(boolean desc) {
        orderBy(WeatherColumns.HUMIDITY, desc);
        return this;
    }

    public WeatherSelection orderByHumidity() {
        orderBy(WeatherColumns.HUMIDITY, false);
        return this;
    }

    public WeatherSelection pressure(float... value) {
        addEquals(WeatherColumns.PRESSURE, toObjectArray(value));
        return this;
    }

    public WeatherSelection pressureNot(float... value) {
        addNotEquals(WeatherColumns.PRESSURE, toObjectArray(value));
        return this;
    }

    public WeatherSelection pressureGt(float value) {
        addGreaterThan(WeatherColumns.PRESSURE, value);
        return this;
    }

    public WeatherSelection pressureGtEq(float value) {
        addGreaterThanOrEquals(WeatherColumns.PRESSURE, value);
        return this;
    }

    public WeatherSelection pressureLt(float value) {
        addLessThan(WeatherColumns.PRESSURE, value);
        return this;
    }

    public WeatherSelection pressureLtEq(float value) {
        addLessThanOrEquals(WeatherColumns.PRESSURE, value);
        return this;
    }

    public WeatherSelection orderByPressure(boolean desc) {
        orderBy(WeatherColumns.PRESSURE, desc);
        return this;
    }

    public WeatherSelection orderByPressure() {
        orderBy(WeatherColumns.PRESSURE, false);
        return this;
    }

    public WeatherSelection wind(float... value) {
        addEquals(WeatherColumns.WIND, toObjectArray(value));
        return this;
    }

    public WeatherSelection windNot(float... value) {
        addNotEquals(WeatherColumns.WIND, toObjectArray(value));
        return this;
    }

    public WeatherSelection windGt(float value) {
        addGreaterThan(WeatherColumns.WIND, value);
        return this;
    }

    public WeatherSelection windGtEq(float value) {
        addGreaterThanOrEquals(WeatherColumns.WIND, value);
        return this;
    }

    public WeatherSelection windLt(float value) {
        addLessThan(WeatherColumns.WIND, value);
        return this;
    }

    public WeatherSelection windLtEq(float value) {
        addLessThanOrEquals(WeatherColumns.WIND, value);
        return this;
    }

    public WeatherSelection orderByWind(boolean desc) {
        orderBy(WeatherColumns.WIND, desc);
        return this;
    }

    public WeatherSelection orderByWind() {
        orderBy(WeatherColumns.WIND, false);
        return this;
    }

    public WeatherSelection degrees(float... value) {
        addEquals(WeatherColumns.DEGREES, toObjectArray(value));
        return this;
    }

    public WeatherSelection degreesNot(float... value) {
        addNotEquals(WeatherColumns.DEGREES, toObjectArray(value));
        return this;
    }

    public WeatherSelection degreesGt(float value) {
        addGreaterThan(WeatherColumns.DEGREES, value);
        return this;
    }

    public WeatherSelection degreesGtEq(float value) {
        addGreaterThanOrEquals(WeatherColumns.DEGREES, value);
        return this;
    }

    public WeatherSelection degreesLt(float value) {
        addLessThan(WeatherColumns.DEGREES, value);
        return this;
    }

    public WeatherSelection degreesLtEq(float value) {
        addLessThanOrEquals(WeatherColumns.DEGREES, value);
        return this;
    }

    public WeatherSelection orderByDegrees(boolean desc) {
        orderBy(WeatherColumns.DEGREES, desc);
        return this;
    }

    public WeatherSelection orderByDegrees() {
        orderBy(WeatherColumns.DEGREES, false);
        return this;
    }
}
