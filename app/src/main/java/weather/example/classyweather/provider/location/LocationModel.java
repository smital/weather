package weather.example.classyweather.provider.location;

import weather.example.classyweather.provider.base.BaseModel;

import java.util.Date;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Data model for the {@code location} table.
 */
public interface LocationModel extends BaseModel {

    /**
     * Get the {@code city_name} value.
     * Cannot be {@code null}.
     */
    @NonNull
    String getCityName();

    /**
     * Get the {@code url_1} value.
     * Can be {@code null}.
     */
    @Nullable
    String getUrl1();

    /**
     * Get the {@code url_2} value.
     * Can be {@code null}.
     */
    @Nullable
    String getUrl2();

    /**
     * Get the {@code url_3} value.
     * Can be {@code null}.
     */
    @Nullable
    String getUrl3();
}
