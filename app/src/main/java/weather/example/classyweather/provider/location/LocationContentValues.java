package weather.example.classyweather.provider.location;

import java.util.Date;

import android.content.Context;
import android.content.ContentResolver;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import weather.example.classyweather.provider.base.AbstractContentValues;

/**
 * Content values wrapper for the {@code location} table.
 */
public class LocationContentValues extends AbstractContentValues {
    @Override
    public Uri uri() {
        return LocationColumns.CONTENT_URI;
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(ContentResolver contentResolver, @Nullable LocationSelection where) {
        return contentResolver.update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(Context context, @Nullable LocationSelection where) {
        return context.getContentResolver().update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    public LocationContentValues putCityName(@NonNull String value) {
        if (value == null) throw new IllegalArgumentException("cityName must not be null");
        mContentValues.put(LocationColumns.CITY_NAME, value);
        return this;
    }


    public LocationContentValues putUrl1(@Nullable String value) {
        mContentValues.put(LocationColumns.URL_1, value);
        return this;
    }

    public LocationContentValues putUrl1Null() {
        mContentValues.putNull(LocationColumns.URL_1);
        return this;
    }

    public LocationContentValues putUrl2(@Nullable String value) {
        mContentValues.put(LocationColumns.URL_2, value);
        return this;
    }

    public LocationContentValues putUrl2Null() {
        mContentValues.putNull(LocationColumns.URL_2);
        return this;
    }

    public LocationContentValues putUrl3(@Nullable String value) {
        mContentValues.put(LocationColumns.URL_3, value);
        return this;
    }

    public LocationContentValues putUrl3Null() {
        mContentValues.putNull(LocationColumns.URL_3);
        return this;
    }
}
