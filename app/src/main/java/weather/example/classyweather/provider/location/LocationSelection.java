package weather.example.classyweather.provider.location;

import java.util.Date;

import android.content.Context;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import weather.example.classyweather.provider.base.AbstractSelection;

/**
 * Selection for the {@code location} table.
 */
public class LocationSelection extends AbstractSelection<LocationSelection> {
    @Override
    protected Uri baseUri() {
        return LocationColumns.CONTENT_URI;
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param contentResolver The content resolver to query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @return A {@code LocationCursor} object, which is positioned before the first entry, or null.
     */
    public LocationCursor query(ContentResolver contentResolver, String[] projection) {
        Cursor cursor = contentResolver.query(uri(), projection, sel(), args(), order());
        if (cursor == null) return null;
        return new LocationCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, null)}.
     */
    public LocationCursor query(ContentResolver contentResolver) {
        return query(contentResolver, null);
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param context The context to use for the query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @return A {@code LocationCursor} object, which is positioned before the first entry, or null.
     */
    public LocationCursor query(Context context, String[] projection) {
        Cursor cursor = context.getContentResolver().query(uri(), projection, sel(), args(), order());
        if (cursor == null) return null;
        return new LocationCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(context, null)}.
     */
    public LocationCursor query(Context context) {
        return query(context, null);
    }


    public LocationSelection id(long... value) {
        addEquals("location." + LocationColumns._ID, toObjectArray(value));
        return this;
    }

    public LocationSelection idNot(long... value) {
        addNotEquals("location." + LocationColumns._ID, toObjectArray(value));
        return this;
    }

    public LocationSelection orderById(boolean desc) {
        orderBy("location." + LocationColumns._ID, desc);
        return this;
    }

    public LocationSelection orderById() {
        return orderById(false);
    }

    public LocationSelection cityName(String... value) {
        addEquals(LocationColumns.CITY_NAME, value);
        return this;
    }

    public LocationSelection cityNameNot(String... value) {
        addNotEquals(LocationColumns.CITY_NAME, value);
        return this;
    }

    public LocationSelection cityNameLike(String... value) {
        addLike(LocationColumns.CITY_NAME, value);
        return this;
    }

    public LocationSelection cityNameContains(String... value) {
        addContains(LocationColumns.CITY_NAME, value);
        return this;
    }

    public LocationSelection cityNameStartsWith(String... value) {
        addStartsWith(LocationColumns.CITY_NAME, value);
        return this;
    }

    public LocationSelection cityNameEndsWith(String... value) {
        addEndsWith(LocationColumns.CITY_NAME, value);
        return this;
    }

    public LocationSelection orderByCityName(boolean desc) {
        orderBy(LocationColumns.CITY_NAME, desc);
        return this;
    }

    public LocationSelection orderByCityName() {
        orderBy(LocationColumns.CITY_NAME, false);
        return this;
    }

    public LocationSelection url1(String... value) {
        addEquals(LocationColumns.URL_1, value);
        return this;
    }

    public LocationSelection url1Not(String... value) {
        addNotEquals(LocationColumns.URL_1, value);
        return this;
    }

    public LocationSelection url1Like(String... value) {
        addLike(LocationColumns.URL_1, value);
        return this;
    }

    public LocationSelection url1Contains(String... value) {
        addContains(LocationColumns.URL_1, value);
        return this;
    }

    public LocationSelection url1StartsWith(String... value) {
        addStartsWith(LocationColumns.URL_1, value);
        return this;
    }

    public LocationSelection url1EndsWith(String... value) {
        addEndsWith(LocationColumns.URL_1, value);
        return this;
    }

    public LocationSelection orderByUrl1(boolean desc) {
        orderBy(LocationColumns.URL_1, desc);
        return this;
    }

    public LocationSelection orderByUrl1() {
        orderBy(LocationColumns.URL_1, false);
        return this;
    }

    public LocationSelection url2(String... value) {
        addEquals(LocationColumns.URL_2, value);
        return this;
    }

    public LocationSelection url2Not(String... value) {
        addNotEquals(LocationColumns.URL_2, value);
        return this;
    }

    public LocationSelection url2Like(String... value) {
        addLike(LocationColumns.URL_2, value);
        return this;
    }

    public LocationSelection url2Contains(String... value) {
        addContains(LocationColumns.URL_2, value);
        return this;
    }

    public LocationSelection url2StartsWith(String... value) {
        addStartsWith(LocationColumns.URL_2, value);
        return this;
    }

    public LocationSelection url2EndsWith(String... value) {
        addEndsWith(LocationColumns.URL_2, value);
        return this;
    }

    public LocationSelection orderByUrl2(boolean desc) {
        orderBy(LocationColumns.URL_2, desc);
        return this;
    }

    public LocationSelection orderByUrl2() {
        orderBy(LocationColumns.URL_2, false);
        return this;
    }

    public LocationSelection url3(String... value) {
        addEquals(LocationColumns.URL_3, value);
        return this;
    }

    public LocationSelection url3Not(String... value) {
        addNotEquals(LocationColumns.URL_3, value);
        return this;
    }

    public LocationSelection url3Like(String... value) {
        addLike(LocationColumns.URL_3, value);
        return this;
    }

    public LocationSelection url3Contains(String... value) {
        addContains(LocationColumns.URL_3, value);
        return this;
    }

    public LocationSelection url3StartsWith(String... value) {
        addStartsWith(LocationColumns.URL_3, value);
        return this;
    }

    public LocationSelection url3EndsWith(String... value) {
        addEndsWith(LocationColumns.URL_3, value);
        return this;
    }

    public LocationSelection orderByUrl3(boolean desc) {
        orderBy(LocationColumns.URL_3, desc);
        return this;
    }

    public LocationSelection orderByUrl3() {
        orderBy(LocationColumns.URL_3, false);
        return this;
    }
}
