package weather.example.classyweather.provider.weather;

import java.util.Date;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import weather.example.classyweather.provider.base.AbstractCursor;
import weather.example.classyweather.provider.location.*;

/**
 * Cursor wrapper for the {@code weather} table.
 */
public class WeatherCursor extends AbstractCursor implements WeatherModel {
    public WeatherCursor(Cursor cursor) {
        super(cursor);
    }

    /**
     * Primary key.
     */
    public long getId() {
        Long res = getLongOrNull(WeatherColumns._ID);
        if (res == null)
            throw new NullPointerException("The value of '_id' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code location_id} value.
     */
    public int getLocationId() {
        Integer res = getIntegerOrNull(WeatherColumns.LOCATION_ID);
        if (res == null)
            throw new NullPointerException("The value of 'location_id' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code city_name} value.
     * Cannot be {@code null}.
     */
    @NonNull
    public String getLocationCityName() {
        String res = getStringOrNull(LocationColumns.CITY_NAME);
        if (res == null)
            throw new NullPointerException("The value of 'city_name' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code url_1} value.
     * Can be {@code null}.
     */
    @Nullable
    public String getLocationUrl1() {
        String res = getStringOrNull(LocationColumns.URL_1);
        return res;
    }

    /**
     * Get the {@code url_2} value.
     * Can be {@code null}.
     */
    @Nullable
    public String getLocationUrl2() {
        String res = getStringOrNull(LocationColumns.URL_2);
        return res;
    }

    /**
     * Get the {@code url_3} value.
     * Can be {@code null}.
     */
    @Nullable
    public String getLocationUrl3() {
        String res = getStringOrNull(LocationColumns.URL_3);
        return res;
    }

    /**
     * Get the {@code date} value.
     */
    public int getDate() {
        Integer res = getIntegerOrNull(WeatherColumns.DATE);
        if (res == null)
            throw new NullPointerException("The value of 'date' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code short_desc} value.
     * Cannot be {@code null}.
     */
    @NonNull
    public String getShortDesc() {
        String res = getStringOrNull(WeatherColumns.SHORT_DESC);
        if (res == null)
            throw new NullPointerException("The value of 'short_desc' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code weather_id} value.
     */
    public int getWeatherId() {
        Integer res = getIntegerOrNull(WeatherColumns.WEATHER_ID);
        if (res == null)
            throw new NullPointerException("The value of 'weather_id' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code current_temp} value.
     */
    public float getCurrentTemp() {
        Float res = getFloatOrNull(WeatherColumns.CURRENT_TEMP);
        if (res == null)
            throw new NullPointerException("The value of 'current_temp' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code min} value.
     */
    public float getMin() {
        Float res = getFloatOrNull(WeatherColumns.MIN);
        if (res == null)
            throw new NullPointerException("The value of 'min' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code max} value.
     */
    public float getMax() {
        Float res = getFloatOrNull(WeatherColumns.MAX);
        if (res == null)
            throw new NullPointerException("The value of 'max' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code humidity} value.
     */
    public float getHumidity() {
        Float res = getFloatOrNull(WeatherColumns.HUMIDITY);
        if (res == null)
            throw new NullPointerException("The value of 'humidity' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code pressure} value.
     */
    public float getPressure() {
        Float res = getFloatOrNull(WeatherColumns.PRESSURE);
        if (res == null)
            throw new NullPointerException("The value of 'pressure' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code wind} value.
     */
    public float getWind() {
        Float res = getFloatOrNull(WeatherColumns.WIND);
        if (res == null)
            throw new NullPointerException("The value of 'wind' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code degrees} value.
     */
    public float getDegrees() {
        Float res = getFloatOrNull(WeatherColumns.DEGREES);
        if (res == null)
            throw new NullPointerException("The value of 'degrees' in the database was null, which is not allowed according to the model definition");
        return res;
    }
}
