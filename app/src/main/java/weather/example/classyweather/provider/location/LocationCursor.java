package weather.example.classyweather.provider.location;

import java.util.Date;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import weather.example.classyweather.provider.base.AbstractCursor;

/**
 * Cursor wrapper for the {@code location} table.
 */
public class LocationCursor extends AbstractCursor implements LocationModel {
    public LocationCursor(Cursor cursor) {
        super(cursor);
    }

    /**
     * Primary key.
     */
    public long getId() {
        Long res = getLongOrNull(LocationColumns._ID);
        if (res == null)
            throw new NullPointerException("The value of '_id' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code city_name} value.
     * Cannot be {@code null}.
     */
    @NonNull
    public String getCityName() {
        String res = getStringOrNull(LocationColumns.CITY_NAME);
        if (res == null)
            throw new NullPointerException("The value of 'city_name' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code url_1} value.
     * Can be {@code null}.
     */
    @Nullable
    public String getUrl1() {
        String res = getStringOrNull(LocationColumns.URL_1);
        return res;
    }

    /**
     * Get the {@code url_2} value.
     * Can be {@code null}.
     */
    @Nullable
    public String getUrl2() {
        String res = getStringOrNull(LocationColumns.URL_2);
        return res;
    }

    /**
     * Get the {@code url_3} value.
     * Can be {@code null}.
     */
    @Nullable
    public String getUrl3() {
        String res = getStringOrNull(LocationColumns.URL_3);
        return res;
    }
}
