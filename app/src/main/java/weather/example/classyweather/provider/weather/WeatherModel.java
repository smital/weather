package weather.example.classyweather.provider.weather;

import weather.example.classyweather.provider.base.BaseModel;

import java.util.Date;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * A product that the company sells.
 */
public interface WeatherModel extends BaseModel {

    /**
     * Get the {@code location_id} value.
     */
    int getLocationId();

    /**
     * Get the {@code date} value.
     */
    int getDate();

    /**
     * Get the {@code short_desc} value.
     * Cannot be {@code null}.
     */
    @NonNull
    String getShortDesc();

    /**
     * Get the {@code weather_id} value.
     */
    int getWeatherId();

    /**
     * Get the {@code current_temp} value.
     */
    float getCurrentTemp();

    /**
     * Get the {@code min} value.
     */
    float getMin();

    /**
     * Get the {@code max} value.
     */
    float getMax();

    /**
     * Get the {@code humidity} value.
     */
    float getHumidity();

    /**
     * Get the {@code pressure} value.
     */
    float getPressure();

    /**
     * Get the {@code wind} value.
     */
    float getWind();

    /**
     * Get the {@code degrees} value.
     */
    float getDegrees();
}
