package weather.example.classyweather.provider;

import android.Manifest;
import android.app.Activity;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;

import timber.log.Timber;
import weather.example.classyweather.R;

import static java.lang.String.format;

/**
 * Created by smitald on 3/12/2016 for ClassyWeather.
 */
public class PlaceInfoProvider implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private static final String TAG = PlaceInfoProvider.class.getSimpleName();
    private PlaceInfoCallback mPlaceInfoCallback;
    private AppCompatActivity mContext;
    private GoogleApiClient mGoogleApiClient;
    private String defaultCity;
    private Place place;

    /**
     * Id to identify a camera permission request.
     */
    private static final int REQUEST_FINE_LOCATION = 0;

    /*
     * Define a request code to send to Google Play services
     * This code is returned in Activity.onActivityResult
     */
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    public PlaceInfoProvider(PlaceInfoCallback mPlaceInfoCallback, AppCompatActivity mContext) {
        this.mPlaceInfoCallback = mPlaceInfoCallback;
        this.mContext = mContext;
    }

    public String getCurrentPlace() {
        String city;
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.


            requestPlaceFetchPermission();

            return null;
        }
        else {
            PendingResult<PlaceLikelihoodBuffer> result = Places.PlaceDetectionApi.getCurrentPlace(mGoogleApiClient, null);

            result.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
                @Override
                public void onResult(PlaceLikelihoodBuffer likelyPlaces) {
                    if (likelyPlaces.getCount() > 0) {
                        defaultCity = likelyPlaces.get(0).getPlace().getName().toString();
                        place = likelyPlaces.get(0).getPlace();
                        mPlaceInfoCallback.handleDefaultPlace(place);
                        Timber.i(format("Place '%s' has likelihood: %g : %s",
                                likelyPlaces.get(0).getPlace().getName(),
                                likelyPlaces.get(0).getLikelihood(), likelyPlaces.get(0).getPlace().getId()));

                        likelyPlaces.release();
                    }
                }
            });
            return defaultCity;
        }
    }

    private void requestPlaceFetchPermission() {
        Log.i(TAG, "CAMERA permission has NOT been granted. Requesting permission.");

        // BEGIN_INCLUDE(camera_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(mContext,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            Log.i(TAG,
                    "Displaying location permission rationale to provide additional context.");

            Snackbar.make(mContext.findViewById(R.id.laylout_label), R.string.permission_location_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(mContext,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    REQUEST_FINE_LOCATION);
                        }
                    })
                    .show();

        } else {

            // Camera permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(mContext, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_FINE_LOCATION);
        }
        // END_INCLUDE(camera_permission_request)
    }


    @Override
    public void onConnected(Bundle bundle) {
        Timber.d("Play services - Places api Connected");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Timber.d("Play services - Places api Connection suspended");
    }

    public interface PlaceInfoCallback {
        void handleDefaultPlace(Place place);
    }

    public void connect() {
        mGoogleApiClient = new GoogleApiClient
                .Builder(mContext)
                .enableAutoManage(mContext, this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }


    public void disconnect() {
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
            mContext = null;
            mPlaceInfoCallback = null;
        }
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Timber.e("Google Api client connection failed");

        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution() && mContext instanceof Activity) {
            try {
                Activity activity = (Activity) mContext;
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(activity, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            /*
             * Thrown if Google Play services canceled the original
             * PendingIntent
             */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            Timber.i("Play services connection failed with code " + connectionResult.getErrorCode());
        }
    }

}
