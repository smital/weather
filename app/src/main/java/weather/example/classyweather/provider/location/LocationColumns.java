package weather.example.classyweather.provider.location;

import android.net.Uri;
import android.provider.BaseColumns;

import weather.example.classyweather.provider.WeatherProvider;
import weather.example.classyweather.provider.location.LocationColumns;
import weather.example.classyweather.provider.weather.WeatherColumns;

/**
 * Columns for the {@code location} table.
 */
public class LocationColumns implements BaseColumns {
    public static final String TABLE_NAME = "location";
    public static final Uri CONTENT_URI = Uri.parse(WeatherProvider.CONTENT_URI_BASE + "/" + TABLE_NAME);

    /**
     * Primary key.
     */
    public static final String _ID = BaseColumns._ID;

    public static final String CITY_NAME = "city_name";

    public static final String URL_1 = "url_1";

    public static final String URL_2 = "url_2";

    public static final String URL_3 = "url_3";


    public static final String DEFAULT_ORDER = TABLE_NAME + "." +_ID;

    // @formatter:off
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            CITY_NAME,
            URL_1,
            URL_2,
            URL_3
    };
    // @formatter:on

    public static boolean hasColumns(String[] projection) {
        if (projection == null) return true;
        for (String c : projection) {
            if (c.equals(CITY_NAME) || c.contains("." + CITY_NAME)) return true;
            if (c.equals(URL_1) || c.contains("." + URL_1)) return true;
            if (c.equals(URL_2) || c.contains("." + URL_2)) return true;
            if (c.equals(URL_3) || c.contains("." + URL_3)) return true;
        }
        return false;
    }

}
