package weather.example.classyweather.provider.weather;

import java.util.Date;

import android.content.Context;
import android.content.ContentResolver;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import weather.example.classyweather.provider.base.AbstractContentValues;

/**
 * Content values wrapper for the {@code weather} table.
 */
public class WeatherContentValues extends AbstractContentValues {
    @Override
    public Uri uri() {
        return WeatherColumns.CONTENT_URI;
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(ContentResolver contentResolver, @Nullable WeatherSelection where) {
        return contentResolver.update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(Context context, @Nullable WeatherSelection where) {
        return context.getContentResolver().update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    public WeatherContentValues putLocationId(int value) {
        mContentValues.put(WeatherColumns.LOCATION_ID, value);
        return this;
    }


    public WeatherContentValues putDate(long value) {
        mContentValues.put(WeatherColumns.DATE, value);
        return this;
    }


    public WeatherContentValues putShortDesc(@NonNull String value) {
        if (value == null) throw new IllegalArgumentException("shortDesc must not be null");
        mContentValues.put(WeatherColumns.SHORT_DESC, value);
        return this;
    }


    public WeatherContentValues putWeatherId(int value) {
        mContentValues.put(WeatherColumns.WEATHER_ID, value);
        return this;
    }


    public WeatherContentValues putCurrentTemp(float value) {
        mContentValues.put(WeatherColumns.CURRENT_TEMP, value);
        return this;
    }


    public WeatherContentValues putMin(float value) {
        mContentValues.put(WeatherColumns.MIN, value);
        return this;
    }


    public WeatherContentValues putMax(float value) {
        mContentValues.put(WeatherColumns.MAX, value);
        return this;
    }


    public WeatherContentValues putHumidity(float value) {
        mContentValues.put(WeatherColumns.HUMIDITY, value);
        return this;
    }


    public WeatherContentValues putPressure(float value) {
        mContentValues.put(WeatherColumns.PRESSURE, value);
        return this;
    }


    public WeatherContentValues putWind(float value) {
        mContentValues.put(WeatherColumns.WIND, value);
        return this;
    }


    public WeatherContentValues putDegrees(float value) {
        mContentValues.put(WeatherColumns.DEGREES, value);
        return this;
    }

}
