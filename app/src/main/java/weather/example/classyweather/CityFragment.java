package weather.example.classyweather;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;
import timber.log.Timber;
import weather.example.classyweather.Receiver.NetworkStateChanged;
import weather.example.classyweather.Utils.NetWorkStatus;
import weather.example.classyweather.Utils.PaletteTransformation;
import weather.example.classyweather.Utils.WeatherConstants;
import weather.example.classyweather.provider.location.LocationColumns;
import weather.example.classyweather.provider.location.LocationCursor;
import weather.example.classyweather.provider.location.LocationSelection;
import weather.example.classyweather.provider.weather.WeatherColumns;
import weather.example.classyweather.provider.weather.WeatherCursor;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CityFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class CityFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, SwipeRefreshLayout.OnRefreshListener {
    private static final int LOADER_ID = 10;
    private static final String TAG = CityFragment.class.getSimpleName();
    private String mCity;
    private int mCityId;
    private OnFragmentInteractionListener mListener;
    private CityAdapter mAdpater;
    private String mCityUrl;
    private ImageView mImageview;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Toolbar toolbar;
    private String mCurrentUrl;
    private ProgressBar mProgressBar;
    private RecyclerView rv;
    private boolean mLoading;
    private int mLastKnownColor;
    private int mLastKnownActionBarColor;
    private LinearLayoutManager mLayoutManager;


    public CityFragment() {
        // Required empty public constructor
        setRetainInstance(true);
    }

    /**
     * This method will be called when the network state changes through broadcast receiver
     *
     * @param event network event
     */
    public void onEvent(NetworkStateChanged event) {
        String test;
        if (!event.isInternetConnected()) {
            Toast.makeText(getActivity(), "No network Detected !!", Toast.LENGTH_LONG).show();
            showerror();
        } else {
            showlist();
            onRefresh();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCity = getArguments().getString(WeatherConstants.CITY_NAME);
            mCityId = getArguments().getInt(WeatherConstants.CITY_ID);
            mCityUrl = getArguments().getString(WeatherConstants.CITY_URL);
        }
        mAdpater = new CityAdapter();
        setRetainInstance(true);
        if (savedInstanceState != null && savedInstanceState.containsKey(WeatherConstants.STATUS_BAR_COLOR)) {
            mLastKnownColor = savedInstanceState.getInt(WeatherConstants.STATUS_BAR_COLOR);
            mLastKnownActionBarColor = savedInstanceState.getInt(WeatherConstants.ACTION_BAR_COLOR);
        } else {
            mLastKnownColor = 0;
            mLastKnownActionBarColor = 0;
            mListener.onFragmentInteraction(mCity);
        }
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(WeatherConstants.STATUS_BAR_COLOR, mLastKnownColor);
        outState.putInt(WeatherConstants.ACTION_BAR_COLOR, mLastKnownActionBarColor);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_city, null, false);
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.cricBar);
        rv = (RecyclerView) rootView.findViewById(R.id.recyclerview);
        setUpRecycler(rv);
        mImageview = (ImageView) rootView.findViewById(R.id.img);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);
        toolbar.setTitle(mCity);
        if (NetWorkStatus.getInstance(getActivity()).isOnline()) {
            showloading();
            loadImage(mCityUrl);
        } else {
            showerror();
        }
        return rootView;
    }


    private void initToolbar() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
    }

    private void setUpRecycler(RecyclerView rv) {
        mLayoutManager = new LinearLayoutManager(rv.getContext());
        rv.setLayoutManager(mLayoutManager);
        rv.setAdapter(mAdpater);
        rv.setHasFixedSize(true);

        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
               int pos =  mLayoutManager.findLastVisibleItemPosition();
               if(pos == 2) {
                   mAdpater.notifyItemChanged(pos);
               }
            }
        });
     }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getLoaderManager().getLoader(LOADER_ID) == null) {
            getLoaderManager().initLoader(LOADER_ID, null, this);
        } else {
            getLoaderManager().restartLoader(LOADER_ID, null, this);
        }
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // Defines a string to contain the selection clause
        String selectionClause = WeatherColumns.LOCATION_ID + " = ?";
        String city_id = String.format("%d", mCityId);

        // Initializes an array to contain selection arguments
        String[] selectionArgs = {city_id};
        return new CursorLoader(getActivity(), WeatherColumns.CONTENT_URI, WeatherColumns.ALL_COLUMNS, selectionClause, selectionArgs, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.d(TAG, "Receiving the updates");
        List<WeatherAdapterModel> weatherList = new ArrayList<>();

        WeatherCursor cursor = new WeatherCursor(data);
        while (cursor.moveToNext()) {
            WeatherAdapterModel model = new WeatherAdapterModel();
            model.setDate(cursor.getDate());
            model.setDegrees(cursor.getDegrees());
            model.setHumidity(cursor.getHumidity());
            model.setWind(cursor.getWind());
            model.setPressure(cursor.getPressure());
            model.setMax(cursor.getMax());
            model.setMin(cursor.getMin());
            model.setWeather_id(cursor.getWeatherId());
            model.setShort_desc(cursor.getShortDesc());
            model.setCurrent_temp(cursor.getCurrentTemp());
            model.setSpeed(cursor.getWind());
            weatherList.add(model);
        }
        if (mLoading) {
            showlist();
        }
        mAdpater.updateDataSet(weatherList);
        mAdpater.notifyDataSetChanged();
    }


    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onRefresh() {
        LocationSelection where = new LocationSelection();
        where.cityName(mCity);
        Cursor c = getActivity().getContentResolver().query(LocationColumns.CONTENT_URI, LocationColumns.ALL_COLUMNS,
                where.sel(), where.args(), null);

        LocationCursor data = new LocationCursor(c);
        data.moveToFirst();
        if (data != null) {
            mCurrentUrl = data.getUrl2();
            loadImage(mCurrentUrl);
        }
        data.close();
        swipeRefreshLayout.setRefreshing(false);
    }

    public void loadImage(String url) {
        Picasso.with(getActivity())
                .load(url)
                .noFade()
                .fit()
                .transform(PaletteTransformation.instance())
                .into(mImageview, new Callback.EmptyCallback() {
                    @Override
                    public void onSuccess() {
                        int max = 0;
                        Bitmap bitmap = ((BitmapDrawable) mImageview.getDrawable()).getBitmap();
                        Palette palette = PaletteTransformation.getPalette(bitmap);
                        Palette.Swatch selectedSwatch = null;
                        for (Palette.Swatch swatch : palette.getSwatches()) {
                            max = Math.max(swatch.getPopulation(), max);
                        }
                        for (Palette.Swatch swatch : palette.getSwatches()) {
                            if (swatch.getPopulation() == max) {
                                selectedSwatch = swatch;
                            }
                        }
                        if (selectedSwatch != null) {
                            mLastKnownActionBarColor = selectedSwatch.getRgb();
                            toolbar.setBackgroundColor(mLastKnownActionBarColor);
                            initToolbar();
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                if (getActivity() != null) {
                                    Window window = getActivity().getWindow();
                                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                                    mLastKnownColor = getDarkColor(mLastKnownActionBarColor);
                                    window.setStatusBarColor(mLastKnownColor);
                                } else {
                                    Timber.d("Activity not found !!");
                                }
                            }
                        } else {
                            Timber.d("Unable to find Swatch");
                        }
                    }
                });
    }

    public static int getDarkColor(int color) {
        int alpha = Color.alpha(color);
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        return Color.argb(alpha, (int) (red * 0.5), (int) (green * 0.5), (int) (blue * 0.5));
    }


    public void setStatusBarColor() {
        if (mLastKnownActionBarColor != 0) {
            toolbar.setBackgroundColor(mLastKnownActionBarColor);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (getActivity() != null) {
                Window window = getActivity().getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(mLastKnownColor);
            } else {
                Timber.d("Activity no found !!");
            }
        }
        if(toolbar != null) {
            toolbar.setTitle(mCity);
        }
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        getActivity().getMenuInflater().inflate(R.menu.frag_menu, menu);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String city);
    }

    @Override
    public void setRetainInstance(boolean retain) {
        super.setRetainInstance(retain);
    }

    public void showloading() {
        mProgressBar.setVisibility(View.VISIBLE);
        mProgressBar.getIndeterminateDrawable().setColorFilter(
                getResources().getColor(R.color.colorAccent),
                android.graphics.PorterDuff.Mode.SRC_IN);
        mLoading = true;
        hidelist();
        hideerror();
    }

    public void showlist() {
        rv.setVisibility(View.VISIBLE);
        hideloading();
        hideerror();
        mLoading = false;
    }

    public void showerror() {
        hideloading();
        mLoading = false;
    }

    public void hideloading() {
        mProgressBar.setVisibility(View.GONE);
    }

    public void hidelist() {
        rv.setVisibility(View.GONE);
    }

    public void hideerror() {
    }
}
